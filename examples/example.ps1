#!/usr/bin/env pwsh
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Create a sample record. If a record with the same identifier already exists,
# no new record will be created.
$RECORD_IDENTIFIER="sample-record"
$TITLE="Sample record"

kadi-apy records create -i "${RECORD_IDENTIFIER}" -t "${TITLE}"

# Add a file to the record, as long as no file with the same name exists yet.
kadi-apy records add-files -R ${RECORD_IDENTIFIER} -n $(Join-Path "${PSScriptRoot}" "test.txt")

# Add an extra metadata entry to the record. Replace a previous value if it
# exists.
kadi-apy records add-metadatum -R ${RECORD_IDENTIFIER} -m length -u km -v 5.0 -t float -f

# Create a sample collection. If a collection with the same identifier already
# exists, no new collection will be created.
$COLLECTION_IDENTIFIER="sample-collection"
$TITLE="Sample collection"

kadi-apy collections create -i "${COLLECTION_IDENTIFIER}" -t "${TITLE}"

# Add the record to the collection, as long as the link does not exist already.
kadi-apy collections add-record-link -C ${COLLECTION_IDENTIFIER} -R ${RECORD_IDENTIFIER}
