#!/usr/bin/env python3
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import pathlib

from kadi_apy import KadiManager


def run():
    """Create some sample resources."""

    # The config file is used to get the PAT and host
    manager = KadiManager()

    # Basic metadata for the new record.
    identifier = "sample-record"
    title = "Sample record"

    # This either gets an existing record or creates a new record if none with the given
    # identifier exists yet. If one exists, but we cannot access it, an exception will
    # be raised.
    record = manager.record(identifier=identifier, title=title, create=True)

    # Name of a file to add to the record, which needs to reside in the current
    # directory.
    filename = "test.txt"

    # Add the file to the record, as long as no file with the same name exists yet.
    path = os.path.join(pathlib.Path(__file__).parent.absolute(), filename)
    record.upload_file(path)

    # Single extra metadatum to add to the record.
    metadatum = {"key": "length", "type": "float", "value": 5.0, "unit": "km"}

    # Add the metadatum to the record. Replace a previous value if it exists.
    record.add_metadatum(metadatum, force=True)

    # Basic metadata for the new collection.
    identifier = "sample-collection"
    title = "Sample collection"

    # Same as for the record above.
    collection = manager.collection(identifier=identifier, title=title, create=True)

    # Add the record to the collection, as long as the link does not exist already.
    collection.add_record_link(record.id)

    print("Sample resources created/updated successfully.")


if __name__ == "__main__":
    run()
