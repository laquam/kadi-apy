Welcome to kadi-apy's documentation!
====================================

|pypi| |license| |zenodo|

.. |pypi| image:: https://img.shields.io/pypi/v/kadi-apy
    :target: https://pypi.org/project/kadi-apy/
    :alt: PyPi

.. |license| image:: https://img.shields.io/pypi/l/kadi-apy
    :target: https://opensource.org/licenses/Apache-2.0
    :alt: License

.. |zenodo| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.4088275.svg
    :target: https://doi.org/10.5281/zenodo.4088275
    :alt: Zenodo

**Kadi-APY** is a library for use in tandem with `Kadi4Mat
<https://gitlab.com/iam-cms/kadi>`__. The REST-like API of Kadi4Mat makes it possible to
programmatically interact with most of the resources that can be used through the web
interface by sending suitable HTTP requests to the different endpoints the API provides.
Detailed information about the API itself can be found in the developer `documentation
<https://kadi4mat.readthedocs.io/en/stable/#httpapi>`__ of Kadi4Mat.

The goal of this library is to make the use of this API as easy as possible. It offers
both an object oriented approach to work with the API in Python as well as a command
line interface (CLI). The library is written in Python 3 and works under both Linux and
Windows. The source code of the project can be found `here
<https://gitlab.com/iam-cms/kadi-apy>`__.

.. toctree::
    :name: setup
    :caption: Setup
    :maxdepth: 1

    setup/installation
    setup/upgrading
    setup/configuration

This sections describes how to install and configure the kadi-apy.

.. toctree::
    :name: usage
    :caption: Usage
    :maxdepth: 1

    usage/lib
    usage/cli_lib
    usage/cli

The library can be used by either directly importing it into any Python code or script
or by using the command line interface (CLI). The library itself is divided into the
sections ``kadi_apy.lib`` and ``kadi_apy.cli.lib``. The former usually returns a
response object as used in the `requests <https://github.com/psf/requests>`__ library.
The latter contains more advanced methods including error handling and print outputs.

.. toctree::
    :name: release-history
    :caption: Release history
    :maxdepth: 2

    HISTORY.md
