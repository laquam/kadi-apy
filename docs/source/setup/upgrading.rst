Upgrading
=========

To upgrade the library, the ``-U`` flag can be used:

.. code-block:: shell

 $ pip3 install kadi-apy -U

When installing from source for development, fetch the latest code first and install it
in editable mode again using the same flag.
