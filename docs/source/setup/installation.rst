Installation
============

The library can be installed using ``pip3`` (or ``pip``, if there is no dedicated
version of ``pip`` for Python 3 installed), which generally comes bundled with Python
installations. Note that Python version >= 3.6 is required.

.. code-block:: shell

    $ pip3 install kadi-apy

If you want to use the CLI, make sure the ``kadi-apy`` command is runnable from
anywhere. Depending on the type of installation and if a virtual environment is used or
not, the ``PATH`` system variable may need to be adapted accordingly to include the path
the executable resides in. For example, the following command can be used on Linux,
assuming the executable resides in ``~/.local/bin``:

.. code-block:: shell

  $ export PATH=${HOME}/.local/bin:${PATH}

This line can also be added to ``.bashrc`` so it will be executed each time a new
terminal is opened:

.. code-block:: shell

  $ echo 'export PATH=${HOME}/.local/bin:${PATH}' >> ${HOME}/.bashrc

On Windows, the ``PATH`` can be modified permanently by editing the respective
environment variable in the advanced system settings of the control panel.

Autocompletion is also available for some shells, namely ``bash``, ``zsh`` and ``fish``,
and can be activated using:

.. code-block:: shell

    $ kadi-apy config activate-autocompletion

Development
-----------

When installing the library from source for development instead, it is recommended to
install the library in editable mode, which simply creates a link to the sources so all
changes are reflected in the installed package immediately. The command will also
install some additional development dependencies as defined in ``setup.py``.

.. code-block:: shell

    $ pip3 install -e .[dev]

Especially for development, running the installation inside a virtual environment is
recommended, see `Virtualenv <https://virtualenv.pypa.io/en/latest/>`__ for more
information.

At this point, it is also recommended to install the `pre-commit
<https://pre-commit.com>`__ hooks by running:

.. code-block:: bash

    pre-commit install

Besides pre-commit, `Pylint <https://www.pylint.org>`__ is used as a static code
analysis tool. It can be used to check the whole project by running:

.. code-block:: bash

    pylint kadi_apy
