CLI
===

The CLI can be used directly in the terminal, inside a shell script or by running it as
an external command inside most programming languages. The first entry point to the CLI
is given by running:

.. code-block:: shell

    $ kadi-apy

All commands concerning different resources are available as various subcommands. For
example, all subcommands to work with records can be listed by running:

.. code-block:: shell

    $ kadi-apy records

The information on how to create a record can be accessed via:

.. code-block:: shell

    $ kadi-apy records create --help

More examples of using the library can be found inside the `examples
<https://gitlab.com/iam-cms/kadi-apy/-/tree/master/examples>`__  directory inside this
project, including a Python, bash and PowerShell script.

.. toctree::
    :maxdepth: 3

    cli/tools
