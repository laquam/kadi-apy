Lib
===

The following shows a python example:

.. code-block:: shell

    $ from kadi_apy import KadiManager

    $ manager = KadiManager()
    $ record = manager.record(identifier="my_identifier", create=True)
    $ record.upload_file("path_to_file")

Note that the functionality in ``kadi_apy.lib`` usually returns a response object as
used in the `requests <https://github.com/psf/requests>`__ library. See the file
`example.py <https://gitlab.com/iam-cms/kadi-apy/-/blob/master/examples/example.py>`__
for an example.

.. toctree::
    :maxdepth: 2

    lib/kadimanager
    lib/resources
