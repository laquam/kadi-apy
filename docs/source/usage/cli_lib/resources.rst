CLIRecord
=========

.. autoclass:: kadi_apy.cli.lib.records.CLIRecord
    :members:
    :inherited-members:
    :show-inheritance:

CLICollection
=============

.. autoclass:: kadi_apy.cli.lib.collections.CLICollection
    :members:
    :inherited-members:
    :show-inheritance:

CLIGroup
========

.. autoclass:: kadi_apy.cli.lib.groups.CLIGroup
    :members:
    :inherited-members:
    :show-inheritance:

CLIUser
=======

.. autoclass:: kadi_apy.cli.lib.users.CLIUser
    :members:
    :inherited-members:
    :show-inheritance:

CLITemplate
===========

.. autoclass:: kadi_apy.cli.lib.templates.CLITemplate
    :members:
    :inherited-members:
    :show-inheritance:

CLIMiscellaneous
================

.. autoclass:: kadi_apy.cli.lib.miscellaneous.CLIMiscellaneous
    :members:
    :inherited-members:

CLISearchResource
=================

.. autoclass:: kadi_apy.cli.search.CLISearchResource
    :members:
    :inherited-members:
    :show-inheritance:

CLISearchUser
==============

.. autoclass:: kadi_apy.cli.search.CLISearchUser
    :members:
    :inherited-members:
    :show-inheritance:

Decorators
==============

.. automodule:: kadi_apy.cli.decorators
    :members:
    :inherited-members:
    :show-inheritance:
