Record
======

.. autoclass:: kadi_apy.lib.resources.records.Record
    :members:
    :inherited-members:

Collection
==========

.. autoclass:: kadi_apy.lib.resources.collections.Collection
    :members:
    :inherited-members:

Group
=====

.. autoclass:: kadi_apy.lib.resources.groups.Group
    :members:
    :inherited-members:

User
====

.. autoclass:: kadi_apy.lib.resources.users.User
    :members:
    :inherited-members:

Template
========

.. autoclass:: kadi_apy.lib.resources.templates.Template
    :members:
    :inherited-members:

Miscellaneous
=============

.. autoclass:: kadi_apy.lib.miscellaneous.Miscellaneous
    :members:
    :inherited-members:

SearchResource
==============

.. autoclass:: kadi_apy.lib.search.SearchResource
    :members:
    :inherited-members:

SearchUser
==========

.. autoclass:: kadi_apy.lib.search.SearchUser
    :members:
    :inherited-members:

Exceptions
===========

.. automodule:: kadi_apy.lib.exceptions
    :members:
    :show-inheritance:
