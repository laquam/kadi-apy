CLI/CLI-Lib
============

More advanced methods, which also include error handling and print outputs, can be found
in ``kadi_apy.cli.lib``. These functions can also be used to write own CLI tools.

.. code-block:: shell

    $ from kadi_apy import CLIKadiManager

    $ manager = CLIKadiManager()
    $ record = manager.record(identifier="my_identifier", create=True)
    $ record.upload_file("path_to_file")

.. toctree::
    :maxdepth: 2

    cli_lib/cli_kadimanager
    cli_lib/resources
