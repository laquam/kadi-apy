# Kadi-APY

**Kadi-APY** is a library for use in tandem with
[Kadi4Mat](https://kadi.iam-cms.kit.edu/ "Kadi4Mat"). The REST-like API of
Kadi4Mat makes it possible to programmatically interact with most of the
resources that can be used through the web interface by sending suitable HTTP
requests to the different endpoints the API provides. Detailed information
about the API itself can be found in the developer
[documentation](https://kadi4mat.readthedocs.io/en/stable/#httpapi) of
Kadi4Mat.

The goal of this library is to make the use of this API as easy as possible. It
offers both an object oriented approach to work with the API in Python as well
as a command line interface (CLI). The library is written in Python 3 and works
under both Linux and Windows.

## Quickstart

### Installation

Install and update using [pip](https://pip.pypa.io/en/stable/getting-started/):

`pip3 install kadi-apy`

Note that Python version >=3.6 is required.

### Configuration

To connect to a Kadi4Mat instance, you need the host and a personal access
token which can be stored in a config file. Run

`kadi-apy config create`

to create a sample config file. See the
[documentation](https://kadi-apy.readthedocs.io/en/stable/setup/configuration.html)
for more details.

### Simple Python example

Upload a file to a record:


```python
from kadi_apy import KadiManager

manager = KadiManager()
record = manager.record(identifier="my_identifier", create=True)
record.upload_file("path_to_file")
```

### Simple CLI example

Upload a file to a record:

`kadi-apy records add-file -R my_identifier -n path_to_file`

## Issues

For any issues regarding the kadi-apy (bugs, suggestions, discussions, etc.)
please use the [issue tracker](https://gitlab.com/iam-cms/kadi-apy/-/issues) of
this project. Before creating an issue, please make sure that no similar issue
is already open and that you are using the latest
[release](https://pypi.org/project/kadi-apy/) or the current version of the
**develop** branch. Note that creating a new issue requires a GitLab account.

For **bugs** in particular, please use the provided `Bug` template when
creating an issue, which also adds the `Bug` label to the issue automatically.

## Contributions

Contributions to the code are always welcome! In order to merge any
contributions back into the main repository, please open a corresponding [merge
request](https://gitlab.com/iam-cms/kadi-apy/-/merge_requests). Please notice
that all merge requests should go to the **develop** branch. You may also add
yourself as a contributor to `AUTHORS.md`.

For instructions on how to set up a development environment, please see the
[documentation](https://kadi-apy.readthedocs.io/en/latest/setup/configuration.html)
for more details.

## Further links

* Source code: https://gitlab.com/iam-cms/kadi-apy
* Releases: https://pypi.org/project/kadi-apy/
* Documentation:
  * Stable (reflecting the latest release): https://kadi-apy.readthedocs.io/en/stable/
  * Latest (reflecting the *develop* branch): https://kadi-apy.readthedocs.io/en/latest/
